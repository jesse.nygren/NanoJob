
function readObject(id, phpURL, task) {
        //Luodaan uusi XMLHttpRequest olio, jolla lähetetään 
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            // Tsekataan, että vastaanottajapää on vastaanottavainen 
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                //Tässä otetaan vastaan php:n lähettämä vastaus
                var response = xmlhttp.responseText;
                //Tässä luodaan muuttuja, johon vastaus tallennetaan ja parsetaan
                
                var ObjArray = JSON.parse(response);
                
                switch(task) {
                    case "test": test(ObjArray);
                    case "myJobs": myJobs(ObjArray);
                }
                
                //console.log(myObj);
                //return myObj;
                //return testArray;
                
               // return response;
            }
        };
        // Tässä tapahtuu itse objektin lähetys, tärkeää huomata, että "php/testaus.php" tilalle laitetaan haluttu php-tiedosto
        xmlhttp.open("POST", phpURL + "?x=" + id, true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send();
    }

    var fill = document.getElementById('spanFill');
    
    document.getElementById('test').addEventListener('click', e => {
        readObject(3, 'php/readWork.php', 'test');
        

        //console.log(objArray[0].header);
        /*
        console.log(objArray);
        
        for(var i = 0; i < objArray; i++) {
            console.log(objArray[i].Header);
        }
        */
        
    });
    
    const btnGetStuff = document.getElementById('btnGetStuff');
    btnGetStuff.addEventListener('click', e => {
        readObject(1, 'php/readWork.php', 'myJobs');
    });
    
    function test(objArray) {
        
        for(var i = 0; i < objArray.length; i++) {
            var h1 = document.createElement('h1');
            var hTxt = document.createTextNode(objArray[i].Header);
            var p = document.createElement('p');
            var pTxt = document.createTextNode(objArray[i].Description);
            
            h1.appendChild(hTxt);
            p.appendChild(pTxt);
            fill.appendChild(h1);
            fill.appendChild(p);
    }
    }
    
    function myJobs(ObjArray) {
        
        for(var i = 0; i < ObjArray.length; i++) {
            var h1 = document.createElement('h1');
            var hTxt = document.createTextNode(ObjArray[i].Header);
            var p = document.createElement('p');
            var pTxt = document.createTextNode(ObjArray[i].Description);
            
            h1.appendChild(hTxt);
            p.appendChild(pTxt);
            fill.appendChild(h1);
            fill.appendChild(p);
    }
    }
    
    
    
    