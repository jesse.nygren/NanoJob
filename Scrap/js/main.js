/*
*Main js file for now.
*
*/

//Get elements from HTML
const divStartMenu = document.getElementById('divStartMenu');
const divMainPage = document.getElementById('divMainPage');
const divFindJob = document.getElementById('divFindJob');
const divOfferJob = document.getElementById('divOfferJob');
const cntFindJob = document.getElementById('cntFindJob');
const cntOfferJob = document.getElementById('cntOfferJob');
const cntAbout = document.getElementById('cntAbout');
const divAbout = document.getElementById('divAbout');
const ulLeftMenu = document.getElementById('ulLeftMenu');

cntFindJob.addEventListener('click', e => {
    //Set displays
    divStartMenu.style.display = 'none';
    divMainPage.style.display = 'initial';
    
    //Set left menu
    createMenuItem('User','#');
    createMenuItem('Settings', '#');
    createMenuItem('About', 'about.html');
    createMenuItem('Throwaway', 'throwaway.html');
    createMenuItem('Services', '#');
    
    viewPage('throwaway.html');
   // document.getElementById("content").innerHTML='<object type="text/html" data="throwaway.html" ></object>';
});

cntOfferJob.addEventListener('click', e => {
    //Set displays
    divStartMenu.style.display = 'none';
    divOfferJob.style.display = 'initial';
});

cntAbout.addEventListener('click', e => {
    //Set displays
    divStartMenu.style.display = 'none';
    divMainPage.style.display = 'initial'
    
    //Generate menu
    createMenuItem('About', 'html/about.html');
    
    viewPage('about.html');
});

function createMenuItem(text, link) {
    var liItem = document.createElement('li');
    var liLink = document.createElement('a');
    var liTxt = document.createTextNode(text);
    liLink.setAttribute('href', '#' );
    liLink.appendChild(liTxt);
    liItem.appendChild(liLink);
    liItem.addEventListener('click', e=> {
        viewPage(link);
    });
    ulLeftMenu.appendChild(liItem);
}

function viewPage(page){
    $('#content').load('html/' + page);    
}
/* JSON 
const formToJSON = elements => [].reduce.call(elements, (data, element) => {
    data[element.name] = element.value;
    return data;
}, {});

const handleFormSubmit = event => {
    // Stop the form from submitting since we’re handling that with AJAX.
    event.preventDefault();
    // Call our function to get the form data.
    const data = formToJSON(form.elements);
    // Demo only: print the form data onscreen as a formatted JSON object.
    const dataContainer = document.getElementsByClassName('results__display')[0];
    // Use `JSON.stringify()` to make the output valid, human-readable JSON.
    dataContainer.textContent = JSON.stringify(data, null, "  ");
};
const form = document.getElementsByClassName('add-new-work')[0];
form.addEventListener('submit', handleFormSubmit);
*/
window.onload = function() {
    divStartMenu.style.display = 'block';

}