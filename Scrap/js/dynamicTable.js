 function readObject(id, phpURL, task) {
     //Luodaan uusi XMLHttpRequest olio, jolla lähetetään 
     var xmlhttp = new XMLHttpRequest();
     xmlhttp.onreadystatechange = function() {
         // Tsekataan, että vastaanottajapää on vastaanottavainen 
         if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
             //Tässä otetaan vastaan php:n lähettämä vastaus
             var response = xmlhttp.responseText;
             //Tässä luodaan muuttuja, johon vastaus tallennetaan ja parsetaan

             var objArray = JSON.parse(response);

             switch (task) {
                 case "test":
                     test(objArray);
             }

         }
     };
     // Tässä tapahtuu itse objektin lähetys, tärkeää huomata, että "php/testaus.php" tilalle laitetaan haluttu php-tiedosto
     xmlhttp.open("POST", phpURL + "?x=" + id, true);
     xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
     xmlhttp.send();
 }


 var fill = document.getElementById('mainContent');

 //var list = document.createElement('Button');


 function test(objArray) {

     var button = [];
     var div = [];
     var pText = [];

     for (var i = 0; i <= objArray.length; i++) {

         button[i] = document.createElement('Button');
         button[i].setAttribute('class', 'accordion');

         div[i] = document.createElement('Div');

         pText[i] = document.createElement('P');



         button[i].innerHTML = "<b>" + objArray[i].Header + "</b>" +
             "<br />" + "Date: " + objArray[i].StartDate +
             "<br />" + "Time: " + objArray[i].StartTime +
             "<br />" + "Salary: " + "<b>" + objArray[i].Salary + "€" + "<b/>";

         if (i % 2 == 0) {
             button[i].style.backgroundColor = "#e6ffcc";
         }
         else {
             button[i].style.backgroundColor = "#ffffcc";
         }
         
         button[i].style.fontSize = 145 + "%";
         button[i].style.color = "black";
         button[i].style.padding = 18 + "px";
         button[i].style.width = 100 + "%";
         button[i].style.textAlign = "centered";
         //button[i].style.border = "none";
         button[i].style.outline = "none";
         button[i].style.transition = 0.4 + "s";

         var divFindJob = document.getElementById('divFindJob');
         divFindJob.appendChild(button[i]);
         button[i].appendChild(div[i]);
         div[i].appendChild(pText[i]);




     };

 };
const aFindJob = document.getElementById('aFindJob');

aFindJob.addEventListener('click', e => {
 readObject(3, 'php/readWork.php', 'test');
});

     
 
 