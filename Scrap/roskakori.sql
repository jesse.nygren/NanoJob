source /home/ubuntu/workspace/testi.sql
jQuery(document).on('ready', readObject(1, 'php/readWork.php', 'myJobs'));

CREATE TABLE COMPETENCY (
    CompetencyID INT NOT NULL,
    Name VARCHAR (30),
    PRIMARY KEY (CompetencyID)
)
CREATE TABLE WORK (
    WorkID INT NOT NULL,
    StartDate DATE,
    EndDate DATE,
    StartTime TIME,
    Duration INT, --in minutes
    CompetencyID INT NOT NULL,
    Booked BOOLEAN NOT NULL,
    Completed BOOL,
    FOREIGN KEY CompetencyID References COMPETENCY(CompetencyID)
)

INSERT INTO WORK VALUES(1,'2017-12-24','2017-12-24', '12:00:00', 30,2,FALSE,'GIVE PRESENTS TO CHILDREN');