DROP DATABASE IF EXISTS NanoJobDB;
CREATE DATABASE NanoJobDB;
USE NanoJobDB;

CREATE TABLE ENDUSER (
    UserID INT NOT NULL AUTO_INCREMENT,
    FirstName VARCHAR (30) NOT NULL,
    LastName VARCHAR (30) NOT NULL,
    PostallAddress VARCHAR (30) NOT NULL,
    ZIPCode CHAR(5),
    City VARCHAR(30),
    PRIMARY KEY (userID)
);
    
CREATE TABLE COMPETENCY (
    CompetencyID INT NOT NULL AUTO_INCREMENT,
    Name VARCHAR (30),
    PRIMARY KEY (CompetencyID)
);

    
CREATE TABLE WORK (
    WorkID INT NOT NULL AUTO_INCREMENT,
    Header VARCHAR (30) NOT NULL,
    EmployerID INT NOT NULL,
    StartDate DATE,
    EndDate DATE,
    StartTime TIME,
    Duration INT,
    Salary INT NOT NULL,
    CompetencyID INT NOT NULL,
    Booked BOOL NOT NULL,
    Completed BOOL,
    Description VARCHAR(200),
    WorkerID INT,
    PRIMARY KEY (WorkID),
    FOREIGN KEY CompetencyID(CompetencyID) REFERENCES COMPETENCY(CompetencyID),
    FOREIGN KEY EmployerID(EmployerID) REFERENCES ENDUSER(UserID),
    FOREIGN KEY WorkerID(WorkerID) REFERENCES ENDUSER(UserID)
);

CREATE TABLE NOTE(
    NoteID INT NOT NULL AUTO_INCREMENT,
    EmployerID INT NOT NULL,
    WorkerID INT,
    Note VARCHAR(500),
    WorkID INT NOT NULL,
    PRIMARY KEY (NoteID),
    FOREIGN KEY EmployerID(EmployerID) REFERENCES WORK(EmployerID),
    FOREIGN KEY WorkerID(WorkerID) REFERENCES WORK(WorkerID),
    FOREIGN KEY WorkID(WorkID) REFERENCES WORK(WorkID)
    
);

CREATE TABLE LOBBY(
    WorkerID INT NOT NULL,
    WorkID INT NOT NULL,
    PRIMARY KEY (WorkerID, WorkID),
    FOREIGN KEY WorkerID(WorkerID) REFERENCES ENDUSER(UserID),
    FOREIGN KEY WorkID(WorkID) REFERENCES WORK(WorkID)
    );

INSERT INTO ENDUSER(FirstName, LastName, PostallAddress, ZIPCode, City)
    VALUES('FirstName', 'LastName', 'PostallAddress', '00001', 'City');    
    
INSERT INTO ENDUSER(FirstName, LastName, PostallAddress, ZIPCode, City) 
    VALUES('FirstName2', 'LastName2', 'PostallAddress2', '00001',
    'City' );
    
INSERT INTO ENDUSER(FirstName, LastName, PostallAddress, ZIPCode, City)
    VALUES('Jesse', 'Nygren', 'Silmupolku 1', '00380', 'Helsinki');

INSERT INTO ENDUSER(FirstName, LastName, PostallAddress, ZIPCode, City)
    VALUES('Mikael', 'Kotkavuori', 'Otaniementie', '12345', 'ESPOO');

INSERT INTO COMPETENCY(Name) VALUES('CLEANER');
INSERT INTO COMPETENCY(Name) VALUES('SANTACLAUS');  
INSERT INTO COMPETENCY(Name) VALUES('WOOD-CHOPPER'); 
    

INSERT INTO WORK(Header, EmployerID, StartDate, EndDate, StartTime, Duration,Salary, CompetencyID, Booked, Completed, Description, WorkerID)
    VALUES('Santaclaus',3,'2017-12-24','2017-12-24', '12:00:00', 30,100,3,FALSE,FALSE,'GIVE PRESENTS TO CHILDREN',2);
INSERT INTO WORK(Header, EmployerID, StartDate, EndDate, StartTime, Duration,Salary, CompetencyID, Booked, Completed, Description, WorkerID)
     VALUES('Cleaning person',2,'2017-12-23','2017-12-24', '11:00:00', 60,100,3,FALSE,FALSE,'Wash dishes',3);
INSERT INTO WORK(Header, EmployerID, StartDate, EndDate, StartTime, Duration,Salary, CompetencyID, Booked, Completed, Description, WorkerID)
     VALUES('Santaclaus',2,'2017-12-24','2017-12-24', '12:30:00', 30,100,3,FALSE,FALSE,'GIVE PRESENTS TO CHILDREN and old people',1);
INSERT INTO WORK(Header, EmployerID, StartDate, EndDate, StartTime, Duration,Salary, CompetencyID, Booked, Completed, Description, WorkerID)
     VALUES('Cleaner',1,'2017-12-24','2017-12-24', '14:00:00', 55,100,3,FALSE,FALSE,'Wash the windows',2);
INSERT INTO WORK(Header, EmployerID, StartDate, EndDate, StartTime, Duration,Salary, CompetencyID, Booked, Completed, Description, WorkerID)
     VALUES('Wood chopping',2,'2017-12-24','2017-12-24', '12:40:00', 30,100,3,FALSE,FALSE,'chop 3 quadratic meters of pine wood',1);
INSERT INTO WORK(Header, EmployerID, StartDate, EndDate, StartTime, Duration,Salary, CompetencyID, Booked, Completed, Description, WorkerID)
     VALUES('Wood cutting',1,'2017-12-24','2017-12-24', '14:40:00', 55,100,3,FALSE,FALSE,'Cut down a big tree',2);


INSERT INTO NOTE(EmployerID, WorkerID, Note, WorkID) VALUES(3, 2, "Moro, hyvin tehty tyot!",1);
INSERT INTO NOTE(EmployerID, WorkerID, Note, WorkID) VALUES(2, 1, "SIIVOA PAREMMIN ENSIKERRALLA PERKELE",2);
INSERT INTO NOTE(EmployerID, WorkerID, Note, WorkID) VALUES(2,1,"LOREM IPSUM",3);
INSERT INTO NOTE(EmployerID, WorkerID, Note, WorkID) VALUES(2,1,"LOREM IPSUM",5);

INSERT INTO LOBBY(WorkerID, WorkID) VALUES(1,1);
INSERT INTO LOBBY(WorkerID, WorkID) VALUES(2,1);
INSERT INTO LOBBY(WorkerID, WorkID) VALUES(3,1);
INSERT INTO LOBBY(WorkerID, WorkID) VALUES(1,2);
INSERT INTO LOBBY(WorkerID, WorkID) VALUES(2,2);
